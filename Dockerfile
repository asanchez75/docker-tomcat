FROM tomcat:8.5.23-jre8
RUN apt-get update && apt-get install vim -y
RUN mkdir -p /data/webapps && cp -Rf /usr/local/tomcat/webapps/* /data/webapps
RUN mkdir -p /data/conf && cp -Rf /usr/local/tomcat/conf/* /data/conf
COPY init.sh /init.sh
CMD ["/bin/bash", "/init.sh"]

